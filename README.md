# 2180608294 - Lê Trần Xuân Kiên #

# UserStory #
| **Title**               | Check information                              |
| ----------------------- | ----------------------------------------------- |
| **Value Statement**     |   -  As a user, I want to go to the subject's detailed information page to make it easier to check |
|                         |   - Go to details page and check                |
| **Acceptance Criteria** |   - When users want to display detailed information, make sure all information is also present |
| **Definition of Done**  |   * Unit Test Passed                            |           
|                         |   * Acceptance Criteria Met                     |
|                         |   * Code Reviewed                               |
|                         |   * Functional Tests Passed                     |
|                         |   * Non-Functional Requirements Met             |
|                         |   * Product Owner Accepts User Story            |
| **Owner**               |    Lê Trần Xuân Kiên                            |


